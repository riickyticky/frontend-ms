<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     protected $url_product_service;
     protected $url_persona_service;

     public function __construct()
    {
        $this->url_product_service = config('constant.product_service');
        $this->url_persona_service = config('constant.persona_service');
    }
}
