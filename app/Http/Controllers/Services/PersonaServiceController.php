<?php

namespace App\Http\Controllers\Services;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Exceptions\ConnectionRefused;
use App\Exceptions\InternalServerError;
use Illuminate\Http\Client\ConnectionException;


class PersonaServiceController extends Controller
{	
	public function leerPersonas(){

        $leerPersonasUri = $this->url_persona_service['url'] . '' . $this->url_persona_service['listar_persona'];
    	$persona = new \stdClass;
        $personasLista = [];
        try {
            
            $response = Http::timeout(1)->get($leerPersonasUri);
            $persona = (object) $response->json();

            if(isset($persona->code) == 500) throw new InternalServerError; //InternalSererError 500

            foreach ($persona as $i => $object) {
                $personasLista[$i] = (object) $object;
            }

        } catch (ConnectionException $e) {
            throw new ConnectionRefused();
        }
    	
        return $personasLista;
    }
}