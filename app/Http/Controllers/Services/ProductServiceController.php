<?php

namespace App\Http\Controllers\Services;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Exceptions\ConnectionRefused;
use App\Exceptions\InternalServerError;
use Illuminate\Http\Client\ConnectionException;


class ProductServiceController extends Controller
{	
	public function leerProductos(){

        $leerProductosUri = $this->url_product_service['url'] . '' . $this->url_product_service['leer_productos'];
    	$productos = new \stdClass;
        $productoObjecto = [];
        try {
            
            $response = Http::timeout(1)->get($leerProductosUri);
            $productos = (object) $response->json();

            if(isset($productos->code) == 500) throw new InternalServerError; //InternalSererError 500

            foreach ($productos as $i => $object) {
                $productoObjecto[$i] = (object) $object;
            }

        } catch (ConnectionException $e) {
            throw new ConnectionRefused();
        }
    	
        return $productoObjecto;
    }
}