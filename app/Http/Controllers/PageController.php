<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Services\ProductServiceController;
use App\Http\Controllers\Services\PersonaServiceController;

class PageController extends Controller
{   
    protected $products;
    protected $persona;

    public function __construct()
    {
        $this->products = new ProductServiceController();
        $this->persona = new PersonaServiceController();
    }

    public function productos(){
        return view('productos.productos', ['productos' => $this->products->leerProductos()]);
    }

    public function mantencion(){
        return view('mantención.mantenciones');
    }

    public function persona(){
        return view('persona.personas',['personas' => $this->persona->leerPersonas()]);   
    }

    public function inicio(){
        return view('persona.personaGeo');
    }

    public function mantencionSinAjax(){
        return view('pruebas.mantencionSinAjax');
    }
}


