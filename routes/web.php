<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'PageController@inicio')->name('inicio');

Route::get('/productos','PageController@productos')->name('productos');

Route::get('/mantenciones','PageController@mantencion')->name('mantencion');

Route::get('/personas','PageController@persona')->name('persona');

Route::get('/sinAjax','PageController@mantencionSinAjax')->name('mantencionSinAjax');
