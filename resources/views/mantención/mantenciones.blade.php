@extends('layout')

@section('seccion')

    <section class="row col-12">
        <div class="col-2"></div><!--NO BORRAR-->
        <div class="col-10">
            <!--Título-->
             <div class="text-uppercase h1 py-3 mt-3 p-1 bg-dark text-white shadow align-items-center d-flex justify-content-between">Mantenciones
                <button type="button" class="btn btn-dark mx-3 my-1 p-0" data-toggle="modal" data-target="#modalAgregar"><h1> + </h1></button>
            </div>
             <!-- Modal -->

           <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Solicitud de Mantención</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                            <div class="row justify-content-around">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Marca Bicicleta:</label>
                                    <input type="text" class="form-control" id="marcaBicicleta">
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Tipo Bicicleta:</label>
                                    <select class="form-control ">
                                        <option value="">Mountain bike</option>
                                        <option value="">BMX</option>
                                        <option value="">Pistera</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row justify-content-around">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Rut:</label>
                                    <input type="text"class="form-control" id="rut">
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Sucursal:</label>
                                    <select class="form-control">
                                        <option value="">Concepción</option>
                                        <option value="">Santiago</option>
                                        <option value="">Tranaquepe</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Detalles</label>
                                <textarea class="form-control" rows="3" id="detalles"></textarea>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="guardar btn btn-success">Guardar</button>
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>

            <!--tabla de mantenciones-->
            <table id="table"class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th class="hidden">ID</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Fecha Ingreso</th>
                        <th scope="col">Fecha Mantencion</th>
                        <th scope="col">Fecha Evaluacion</th>
                        <th scope="col">Fecha Termino</th>
                        <th scope="col">Bicicleta</th>
                        <th scope="col">Detalles</th>
                        <th scope="col">Flujo</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
            </table>

                
        </div><!--fin contenedor de tablas-->
    </section>

    <script type="text/javascript">
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Access-Control-Allow-Origin': '*',
                'Authorization': 'Bearer ',
                'Content-Type': 'application/json; charset=utf-8',
            }
        });


        $(document).ready( function () {
            var tbl;
            tbl = cargarTabla();
            guardarMantencion(tbl);
        } );

        function cargarTabla(){
            var url = '{{ config('constant.mantencion_service.url') }}' + 
                      '{{ config('constant.mantencion_service.listar_mantencion_personalizada') }}';
                 tbl = $('#table').DataTable( {
                        language: {
                            lengthMenu: "Mostrar _MENU_ registro por página",
                            zeroRecords: "Sin datos disponibles",
                            info: "Mostrando página _PAGE_ de _PAGES_",
                            infoEmpty: "Sin datos disponibles",
                            infoFiltered: "(filtro de _MAX_ máximo registro)"
                        },
                        ajax: {
                            url: url,
                            type: 'GET',
                            dataType: "json",
                            dataSrc: "",
                            timeout: 1000,
                            error: function(request, textStatus, errorThrown)
                            {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Ups!',
                                    text: 'Servicio caido',
                                    });
                                tbl.destroy();
                            }
                        },
                        columns: [  
                                    {data : "id", 
                                        class:"hidden"
                                    },
                                    {data : "nombrePersona",},
                                    {data : "fechaIngreso", width: "50px",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaMantencion", width: "80px",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaEvaluacion",width: "50px",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaTermino",width: "50px",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "nombreBicicleta"},
                                    {data : "detallesBicicleta",  width: "50px"},
                                    {data : "flujoMantencion"},
                                    {data : "null",
                                       defaultContent:'<select id="dropdownList" class="dropdown form-control">'+
                                                        '<option value="0" selected disabled>Cambiar flujo..</option>'+
                                                        '<option value="1">Ingreso</option>'+
                                                        '<option value="2">Mantención</option>'+
                                                        '<option value="3">Evaluación</option>'+
                                                        '<option value="4">Termino</option>'+
                                                    '</selec>'
                                    }           
                                ],initComplete: function()
                                {   

                                    $('#table').on('change', 'select.dropdown', function(event){
                                        var flujoId = $(this).val();
                                        var mantencionId = $(this).closest("tr").children("td:first").text();


   var url = '{{ config('constant.mantencion_service.url') }}' + 
             '{{ config('constant.mantencion_service.actualizar_mantencion_por_id') }}'+mantencionId+', '+flujoId;

                                         console.log(url);



                                         $.ajax({
                                            headers: { 
                                                'X-CSRF-TOKEN': $('input[name="_token"]').val(),
                                                'Access-Control-Allow-Origin': '*',
                                                'Authorization': 'Bearer ' 
                                            },
                                            url: url,
                                            type: 'PUT',
                                            dataType: 'JSON',
                                            success: function(result) {
                                                Swal.fire(
                                                    '¡Enviado!',
                                                    'El flujo cambio de forma correcta :D',
                                                    'success'
                                                ).then(function () {
                                                    tbl.destroy();
                                                    cargarTabla();
                                                })
                                            },beforeSend: function(){
                                                  
                                            }
                                        }).fail(function() {
                                            Swal.fire(
                                                '¡Error!',
                                                'No se pudo actualizar el flujo de la mantencion :(, favor intente mas tarde',
                                                'error'
                                            )
                                        });
                                    });
                                      
                                }
                    });

            return tbl;
        }


        function guardarMantencion(tbl){
            $('#modalAgregar').on('show.bs.modal', function() {
    
            $(this).on("click", ".guardar", function(){
                  var marcaBicileta = $('#marcaBicicleta').val();
                  console.log(marcaBicicleta);
                  var rut = $('#rut').val();
                  var detalles = $('#detalles').val();

                  var hoy = new Date();

                  //crear objeto personas
                  var mantencion = new Object();
                  mantencion.personaId = 72;
                  mantencion.sucursalId = 1;
                  mantencion.activo = 1;
                  mantencion.fechaIngreso = hoy.toLocaleDateString("en-US");
                  mantencion.bicicleta = new Object();
                  mantencion.bicicleta.nombreBicicleta = marcaBicileta;
                  mantencion.bicicleta.activo = 1;
                  mantencion.bicicleta.detalles = detalles;
                  mantencion.bicicleta.tipoBicicleta = new Object();
                  mantencion.bicicleta.tipoBicicleta.nombreTipoBicicleta = "a";
                  mantencion.bicicleta.tipoBicicleta.activo = 1;
                  mantencion.flujoMantencion = new Object();
                  mantencion.flujoMantencion.id = 1;

                  console.log(mantencion);

                  var mantencionJSON = JSON.stringify(mantencion);

                var url = '{{ config('constant.mantencion_service.url') }}' + '{{ config('constant.mantencion_service.crear_mantencion') }}';

                  $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: mantencionJSON,
                    success: function(result) {
                        Swal.fire(
                            '¡Exito!',
                            'Mantención ingresada, puede ver el estado en "Mis mantenciones"',
                            'success'
                            ).then(function () {
                                $('#modalAgregar').modal('hide');
                                tbl.destroy();
                                cargarTabla();
                            })
                        },beforeSend: function(){
                            //Hacer alguna wea
                      }
                  }).fail(function() {
                    Swal.fire(
                        '¡Error!',
                        'No se pudo crear la mantencion, verifique los datos ingresado o vuelva a intenterlo mas tarde',
                        'error'
                        )
                });
            });
       
    })
        }
        

    </script>

@endsection
