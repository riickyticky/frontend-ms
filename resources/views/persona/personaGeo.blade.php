@extends('layout')

@section('seccion')

    <section class="row col-12">
        <div class="col-2"></div><!--NO BORRAR-->
        <div class="col-10">
            <!--Título-->
             <div class="text-uppercase h1 py-3 mt-3 p-1 bg-dark text-white shadow align-items-center d-flex justify-content-between">Mantenciones
            </div>
            

            <!--tabla de mantenciones-->
            <table id="table"class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                
                        <th scope="col">Persona</th>
                        <th scope="col">Rut</th>
                        <th scope="col">Email</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Region</th>
                    </tr>
                </thead>
            </table>

                
        </div><!--fin contenedor de tablas-->
    </section>

    <script type="text/javascript">
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Access-Control-Allow-Origin': '*',
                'Authorization': 'Bearer ',
                'Content-Type': 'application/json; charset=utf-8',
            }
        });


        $(document).ready( function () {
            var tbl;
            tbl = cargarTabla();
            guardarMantencion(tbl);
        } );

        function cargarTabla(){
            var url = "http://localhost:8051/api/persona-geo";
                 tbl = $('#table').DataTable( {
                        language: {
                            lengthMenu: "Mostrar _MENU_ registro por página",
                            zeroRecords: "Sin datos disponibles",
                            info: "Mostrando página _PAGE_ de _PAGES_",
                            infoEmpty: "Sin datos disponibles",
                            infoFiltered: "(filtro de _MAX_ máximo registro)"
                        },
                        ajax: {
                            url: url,
                            type: 'GET',
                            dataType: "json",
                            dataSrc: "",
                            timeout: 1000,
                            error: function(request, textStatus, errorThrown)
                            {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Ups!',
                                    text: 'Servicio caido',
                                    });
                                tbl.destroy();
                            }
                        },
                        columns: [  
                                    {data : "nombreCompletoPersona",},
                                    {data : "rut",},
                                    {data : "email",},
                                    {data : "nombreDireccion",},
                                    {data : "nombreRegion",},
                                    
                                ],initComplete: function()
                                {   

                                    $('#table').on('change', 'select.dropdown', function(event){
                                        var flujoId = $(this).val();
                                        var mantencionId = $(this).closest("tr").children("td:first").text();


   var url = '{{ config('constant.mantencion_service.url') }}' + 
             '{{ config('constant.mantencion_service.actualizar_mantencion_por_id') }}'+mantencionId+', '+flujoId;

                                         console.log(url);



                                         $.ajax({
                                            headers: { 
                                                'X-CSRF-TOKEN': $('input[name="_token"]').val(),
                                                'Access-Control-Allow-Origin': '*',
                                                'Authorization': 'Bearer ' 
                                            },
                                            url: url,
                                            type: 'PUT',
                                            dataType: 'JSON',
                                            success: function(result) {
                                                Swal.fire(
                                                    '¡Enviado!',
                                                    'El flujo cambio de forma correcta :D',
                                                    'success'
                                                ).then(function () {
                                                    tbl.destroy();
                                                    cargarTabla();
                                                })
                                            },beforeSend: function(){
                                                  
                                            }
                                        }).fail(function() {
                                            Swal.fire(
                                                '¡Error!',
                                                'No se pudo actualizar el flujo de la mantencion :(, favor intente mas tarde',
                                                'error'
                                            )
                                        });
                                    });
                                      
                                }
                    });

            return tbl;
        }


        function guardarMantencion(tbl){
            $('#modalAgregar').on('show.bs.modal', function() {
    
            $(this).on("click", ".guardar", function(){
                  var marcaBicileta = $('#marcaBicicleta').val();
                  console.log(marcaBicicleta);
                  var rut = $('#rut').val();
                  var detalles = $('#detalles').val();

                  var hoy = new Date();

                  //crear objeto personas
                  var mantencion = new Object();
                  mantencion.personaId = 72;
                  mantencion.sucursalId = 1;
                  mantencion.activo = 1;
                  mantencion.fechaIngreso = hoy.toLocaleDateString("en-US");
                  mantencion.bicicleta = new Object();
                  mantencion.bicicleta.nombreBicicleta = marcaBicileta;
                  mantencion.bicicleta.activo = 1;
                  mantencion.bicicleta.detalles = detalles;
                  mantencion.bicicleta.tipoBicicleta = new Object();
                  mantencion.bicicleta.tipoBicicleta.nombreTipoBicicleta = "a";
                  mantencion.bicicleta.tipoBicicleta.activo = 1;
                  mantencion.flujoMantencion = new Object();
                  mantencion.flujoMantencion.id = 1;

                  console.log(mantencion);

                  var mantencionJSON = JSON.stringify(mantencion);

                var url = '{{ config('constant.mantencion_service.url') }}' + '{{ config('constant.mantencion_service.crear_mantencion') }}';

                  $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'JSON',
                    data: mantencionJSON,
                    success: function(result) {
                        Swal.fire(
                            '¡Exito!',
                            'Mantención ingresada, puede ver el estado en "Mis mantenciones"',
                            'success'
                            ).then(function () {
                                $('#modalAgregar').modal('hide');
                                tbl.destroy();
                                cargarTabla();
                            })
                        },beforeSend: function(){
                            //Hacer alguna wea
                      }
                  }).fail(function() {
                    Swal.fire(
                        '¡Error!',
                        'No se pudo crear la mantencion, verifique los datos ingresado o vuelva a intenterlo mas tarde',
                        'error'
                        )
                });
            });
       
    })
        }
        

    </script>

@endsection
