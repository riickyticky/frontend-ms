@extends('layout')



@section('seccion')


   <div class="row col-12  ">
        <div class="col-2"></div><!--NO BORRAR-->
        <!--Título-->
        <div class="col 10">
            <div class="h1 text-uppercase py-3 mt-3 p-1 bg-dark text-white shadow d-flex justify-content-between">Personas
                    <button type="button" class="btn btn-dark mx-3 my-1 p-0" data-toggle="modal" data-target="#modalAgregar"><h1> + </h1></button>
                </div> 
                
                <!-- Modal -->

                <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Agregar Persona</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div><!--modal-header-->
                            <div class="modal-body">
                                <form>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Nombre completo:</label>
                                    <input type="text" class="form-control" id="nombrePersona">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Rut:</label>
                                    <input type="text" class="form-control" id="rut">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Email:</label>
                                    <input type="email" class="form-control" id="email">
                                </div>
                                <div class="form-group">
                                    <label for="message-text" class="col-form-label">Password:</label>
                                    <input type="text" class="form-control" id="password">
                                </div>

                                </form>
                            </div><!--modal-body-->
                            <div class="modal-footer">
                                
                                <button type="button" class="guardar btn btn-success">Guardar</button>
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
                            </div><!--modal-footer-->
                        </div><!--modal-content-->
                    </div>
                </div><!-- fin modal-->
                
            </div><!--contenedor del titulo y el modal-->
        </div>
   </div>
    
    
    <section class="row col-12">
       <div class="col-2"></div><!--NO BORRAR-->
        <!--contenedor de personas-->
        <div class="row justify-content-center col-10 col-sm">
            @foreach($personas as $p)
            <div class="card col-3 mx-3 mb-3 bg-dark text-white" style="width: 18rem;">
                <img src="{{$p->urlImg}}" class="card-img-top p-3 shadow bg-white mt-3" alt="..." style=" height: 18rem;">
                
                <div class="card-body">
                    <h4 class="card-title text-center font-weight-bold">{{$p->nombrePersona}}</h4>
                    <h4 class="card-title text-center font-weight-bold">{{$p->rut}}</h4>
                    <p class="card-text lead">
                        <span class="font-weight-bold">Email: </span> {{$p->usuario['email']}}
                    </p> 
                    <p class="card-text lead">
                        <span class="font-weight-bold">Direccion: </span> Las Lilas
                    </p>
                    <p class="card-text lead">
                        <span class="font-weight-bold">Usuario: </span> {{$p->usuario['nombreUsuario']}}
                    </p> 
                    <p class="card-text lead">
                        <span class="font-weight-bold">Tipo Usuario: </span> {{$p->usuario['tipoUsuario']['nombreTipoUsuario']}}
                    </p>  
                </div>

            </div>
             @endforeach
        </div><!--contenedor de cartas-->
   
    </section>

    <script type="text/javascript">

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'Access-Control-Allow-Origin': '*',
                'Authorization': 'Bearer ',
                'Content-Type': 'application/json; charset=utf-8',
            }
        });

    $('#modalAgregar').on('show.bs.modal', function() {
    
        $(this).on("click", ".guardar", function(){
              var nombrePersona = $('#nombrePersona').val();
              var password = $('#password').val();
              var rut = $('#rut').val();
              var email = $('#email').val();

              //crear nombre usuario
              var nombrePersonaSplit = nombrePersona.split(" ");
              var nombreUsuario = nombrePersonaSplit[0] + "." + nombrePersonaSplit[1];

              //crear objeto personas
              var persona = new Object();
              persona.nombrePersona = nombrePersona;
              persona.rut = rut;
              persona.urlImg = "https://img.favpng.com/2/12/12/computer-icons-portable-network-graphics-user-profile-avatar-png-favpng-L1ihcbxsHbnBKBvjjfBMFGbb7.jpg";
              persona.usuario = new Object();
              persona.usuario.nombreUsuario = nombreUsuario.toLowerCase();
              persona.usuario.email = email;
              persona.usuario.contraseña = password;
              persona.usuario.tipoUsuario = new Object();
              persona.usuario.tipoUsuario.id = 2;

              console.log(persona);

              var personaJSON = JSON.stringify(persona);

              var url = '{{ config('constant.persona_service.url') }}' + '{{ config('constant.persona_service.crear_persona') }}';

              $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: personaJSON,
                success: function(result) {
                    Swal.fire(
                        '¡Creado!',
                        'Eres parte de nosotros, Bienvenido :)',
                        'success'
                        ).then(function () {
                            location.reload();
                        })
                    },beforeSend: function(){
                        //Hacer alguna wea
                  }
              }).fail(function() {
                Swal.fire(
                    '¡Error!',
                    'No se pudo crear a la persona, intentelo mas tarde',
                    'error'
                    )
            });
        });
       
    })

</script>

@endsection
