

@extends('layout')



@section('seccion')


   <div class="row col-12  ">
        <div class="col-2"></div><!--NO BORRAR-->
        <!--Título-->
        <div class="col 10">
            <div class="h1 text-uppercase py-3 mt-3 p-1 bg-dark text-white shadow d-flex justify-content-between">Productos
                <button type="button" class="btn btn-dark mx-3 my-1 p-0" data-toggle="modal" data-target="#modalAgregar"><h1> + </h1></button>
            </div> 
            
            <!-- Modal -->

            <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Agregar producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div><!--modal-header-->
                        <div class="modal-body">
                            <form>
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Nombre:</label>
                                <input type="text" class="form-control" id="recipient-name">
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Precio:</label>
                                <textarea class="form-control" id="message-text"></textarea>
                            </div>
                            </form>
                        </div><!--modal-body-->
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-success">Guardar</button>
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
                        </div><!--modal-footer-->
                    </div><!--modal-content-->
                </div>
            </div><!-- fin modal-->
            
        </div><!--contenedor del titulo y el modal-->

        
   </div><!--contenedor principal del header-
    
    
    <section class="row col-12">
       <div class="col-2"></div><!--NO BORRAR-->
        <!--contenedor de productos-->
        <div class="row justify-content-center col-10 col-sm">
            @if($productos != null)
            @foreach($productos as $p)
            <div class="card col-3 mx-3 mb-3 bg-dark text-white" style="width: 18rem;">
                <img src="https://rodastockchile.cl/wp-content/uploads/2018/04/rodamientos-NKE-rodillos-de-leva.jpg" class="card-img-top p-3 shadow bg-white mt-3" alt="..." style=" height: 18rem;">
                <div class="card-body">
                    <h4 class="card-title text-center font-weight-bold">{{ $p->nombreProducto }}</h4>
                    <p class="card-text lead"><span class="font-weight-bold">Tipo: </span> {{ $p->tipoProducto['nombreTipoProducto'] }}</p>
                    <span class="font-weight-bold">Precio: $</span> {{ $p->precioProducto }}</p>
                    
                </div>
            </div>
            @endforeach
            @else
            <div class="card col-3 mx-3 mb-3 bg-dark text-white" style="width: 18rem;">
                <img src="https://images.emojiterra.com/google/android-pie/512px/2639.png" class="card-img-top p-3 shadow bg-white mt-3" alt="..." style=" height: 18rem;">
                <div class="card-body">
                    <span class="font-weight-bold">No hay productos :(</span></p>   
                </div>
            </div>
            @endif
        </div><!--contenedor de cartas-->
    
    </section>

@endsection