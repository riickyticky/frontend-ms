@extends('layout')

@section('seccion')

    <section class="row col-12">
        <div class="col-2"></div><!--NO BORRAR-->
        <div class="col-10">
            <!--Título-->
            <div class="text-uppercase h1 py-3 mt-3 p-1 bg-dark text-white shadow align-items-center d-flex justify-content-between">Mantenciones
                <button type="button" class="btn btn-dark mx-3 my-1 p-0" data-toggle="modal" data-target="#modalAgregar"><h1> + </h1></button>
            </div>
             <!-- Modal -->

            <div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Solicitud de Mantención</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                            <div class="row justify-content-around">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Marca Bicicleta:</label>
                                    <input type="text" class="form-control">
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Tipo Bicicleta:</label>
                                    <select class="form-control ">
                                        <option value="">Mountain bike</option>
                                        <option value="">BMX</option>
                                        <option value="">Pistera</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row justify-content-around">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Rut:</label>
                                    <input type="text"class="form-control">
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Sucursal:</label>
                                    <select class="form-control">
                                        <option value="">Concepción</option>
                                        <option value="">Santiago</option>
                                        <option value="">Tranaquepe</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Detalles</label>
                                <textarea class="form-control" rows="3"></textarea>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            
                            <button type="button" class="btn btn-success">Guardar</button>
                            <button type="button" class="btn btn-dark" data-dismiss="modal">Cancelar</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--tabla de mantenciones-->
            <table id="table"class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Cliente</th>
                        <th scope="col">Fecha Ingreso</th>
                        <th scope="col">Fecha Mantencion</th>
                        <th scope="col">Fecha Evaluacion</th>
                        <th scope="col">Fecha Termino</th>
                        <th scope="col">Bicicleta</th>
                        <th scope="col">Flujo</th>
                        <th scope="col">Estado</th> 
                    </tr>
                </thead>
            </table>

                
        </div><!--fin contenedor de tablas-->
    </section>

    <script type="text/javascript">

        var listaDeMantenciones;

        $(document).ready( function () {
              listaDeMantenciones = [
                                        {
                                            'personaId':'Ricardo',
                                            'fechaIngreso':'2020-01-01',
                                            'fechaMantencion':'----',
                                            'fechaEvaluacion':'---',
                                            'fechaTermino':'---',
                                            'bicicleta':'BMX',
                                            'flujoMantencion':'Ingreso',
                                        },
                                        {
                                            'personaId':'Fabian',
                                            'fechaIngreso':'2020-01-01',
                                            'fechaMantencion':'----',
                                            'fechaEvaluacion':'---',
                                            'fechaTermino':'---',
                                            'bicicleta':'ARO222',
                                            'flujoMantencion':'Ingreso',
                                        },
                                  ];
            cargarTabla();
        } );

        function cargarTabla(){
                var estados = {1: "ingreso", 2: "mantencion", 3: "evaluación", 4: "termino"};
                var tbl = $('#table').DataTable( {
                        data: listaDeMantenciones,
                        columns: [  
                                    {data : "personaId",},
                                    {data : "fechaIngreso", 
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaMantencion", 
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaEvaluacion",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "fechaTermino",
                                        render: function ( data, type, row ) {
                                           return (data == null)?"---":data;
                                        }
                                    },
                                    {data : "bicicleta"},
                                    {data : "flujoMantencion"},
                                    {data : "estados",
                                        render: function (data, type, row){
                                            return '<select class="form-control"><option>Ingreso</option><option>Mantención</option><option>Evaluación</option><option>Termino</option></selec>';}
                                    }
                                  
                                ]
                    });
        }

    </script>

@endsection
