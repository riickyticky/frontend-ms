<?php

return [

   'product_service' => [
        'url' => env('PRODUCT_SERVICE', ''),
        'leer_productos' => 'api/leer-productos',
   ],

   'persona_service' => [
        'url' => env('PERSONA_SERVICE', ''),
        'crear_persona' => 'api/crear-persona',
        'listar_persona' => 'api/leer-personas',
   ],

   'mantencion_service' => [
        'url' => env('MANTENCION_SERVICE', ''),
        'leer_mantencion_bicicleta' => 'api/leer-mantencion-bicicleta',
        'listar_mantencion_personalizada' => 'api/rest/listar-mantenciones-custom',
        'actualizar_mantencion_por_id' => 'api/actualizar-mantencion/',
        'crear_mantencion' => 'api/crear-mantencion-bicicleta'
   ]

];
